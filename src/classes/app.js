import { Sidebar } from './sidebar';
import { Site } from './site';

export class App {
	defaultOptions = {
		site: '#site',
		sidebar: '#sidebar',
	};
	
	constructor( model, options = {} ) {
		this.model = model;
		this.options = {
			...this.defaultOptions,
			...options,
		};
	}
	
	init() {
		const site = new Site( this.options.site );
		site.render( this.model );
		
		const updateCallback = newBlock => {
			this.model.push( newBlock );
			site.render( this.model );
		};
		
		new Sidebar( this.options.sidebar, updateCallback );
	}
}
