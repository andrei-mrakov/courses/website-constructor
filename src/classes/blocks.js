import { col, row, toCss } from '../utils';

export class Block {
	constructor( value, options = {} ) {
		this.value = value;
		this.options = options;
	}
	
	toHtml() {
		throw new Error( 'No toHtml method.' );
	}
}

export class Title extends Block {
	constructor( value, options ) {
		super( value, options );
	}
	
	toHtml() {
		const { tag, styles } = this.options;
		return row( col( `<${tag}>${ this.value }</${tag}>` ), toCss( styles ) );
	}
}

export class Text extends Block {
	constructor( value, options ) {
		super( value, options );
	}
	
	toHtml() {
		return row( col( `<p>${ this.value }</p>` ), toCss( this.options.styles ) );
	}
}

export class Columns extends Block {
	constructor( value, options ) {
		super( value, options );
	}
	
	toHtml() {
		return row( this.value.map( col ).join( '' ), toCss( this.options.styles ) );
	}
}


export class Image extends Block {
	constructor( value, options ) {
		super( value, options );
	}
	
	toHtml() {
		const { alt = 'image', imgStyles, styles } = this.options;
		return row( `<img src="${ this.value }" style="${ toCss( imgStyles ) }" alt="${ alt }">`, toCss( styles ) );
	}
}
