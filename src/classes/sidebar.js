import { blockForm } from '../utils';
import { Text, Title } from './blocks';

export class Sidebar {
	constructor( selector, updateCallback ) {
		this.$el = document.querySelector( selector );
		this.update = updateCallback;
		if( !this.$el ) {
			throw new Error( 'No sidebar element found.' );
		}
		
		this.init();
	}
	
	init() {
		this.$el.insertAdjacentHTML(
			'beforeend',
			this.template,
		);
		this.$el.addEventListener(
			'submit',
			this.add.bind( this ),
		);
	}
	
	get template() {
		return [
			blockForm( 'text' ),
			blockForm( 'title' ),
		].join( '' );
	}
	
	add( event ) {
		event.preventDefault();
		console.dir( event.target );
		const type = event.target.name;
		const value = event.target.value.value;
		const styles = event.target.styles.value;
		
		const newBlock = type === 'text'
			? new Text( value, { styles } )
			: new Title( value, { styles } );
		
		this.update( newBlock );
		
		event.target.value.value = '';
		event.target.styles.value = '';
	}
}
