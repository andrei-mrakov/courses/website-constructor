export class Site {
	constructor( selector ) {
		this.$el = document.querySelector( selector );
		
		if( !this.$el ) {
			throw new Error( 'No root element found.' );
		}
	}
	
	render(model) {
		this.$el.innerHTML = '';
		model.forEach( block => {
			this.$el.insertAdjacentHTML(
				'beforeend',
				block.toHtml(),
			);
		} );
	}
}
