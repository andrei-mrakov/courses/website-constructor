export function row( content, styles = '' ) {
	return `<div class="row" style="${ styles }">${ content }</div>`;
}

export function col( content, styles = '' ) {
	return `<div class="col-sm" style="${ styles }">${ content }</div>`;
}

export function toCss( styles = {} ) {
	if( typeof styles === 'string' ) {
		return styles;
	}
	return Object.keys( styles ).map( k => `${ k }:${ styles[k] };` ).join( ' ' );
}

export function blockForm( type ) {
	return `
	    <form name="${ type }">
	        <h5>${ type }</h5>
		        <div class="input-container">
		            <input class="input" name="value" placeholder="value">
		        </div>
		        <div class="input-container">
		            <input class="input" name="styles" placeholder="styles">
		        </div>
	        <button class="btn">Добавить</button>
	    </form>
  `;
}
